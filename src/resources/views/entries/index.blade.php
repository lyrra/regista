@extends('layouts.app')
@section('title', 'Regista')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ __(session('status')) }}
                </div>
            @endif

            <div class="card mb-3">
                <div class="card-body">
                    <a class="btn btn-primary btn-lg btn-block mt-3" href="/regista/entries/create" role="button">
                        Checkout
                    </a>
<!-- 
                    <button type="button" class="btn btn-primary btn-lg btn-block mt-3">
                        Register New Entry
                    </button>
 -->                    
                </div>
            </div>

            @if ($entries->isEmpty())
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-subtitle">List is empty</h5>
                    </div>
                </div>
            @endif
            @foreach($entries as $entry)
                <div class="card mb-2 card-block">
                    <div class="card-header">{{ $entry->person->name }}</div>
                    <a href="{{ url('/regista/entries/'.$entry->id) }}" style="color:black;text-decoration:none;">
                        <div class="card-body" style="background-color: white;"
                            onmouseover="this.style.backgroundColor='Lavender';"
                            onmouseout="this.style.backgroundColor='white';">
                            <!-- <h5 class="card-title">Title</h5> -->
                            <h6 class="card-subtitle text-muted">
                                {{ Carbon\Carbon::parse($entry->start_time)->format('M d, Y h:i A') }} - {{ Carbon\Carbon::parse($entry->end_time)->format('h:i A') }}
                            </h6>
                            <p class="card-text">{{ $entry->summary }}</p>
                            <!-- <a class="card-link" href="#">Link</a> -->
                        </div>                        
                    </a>
                    <div class="card-footer text-muted">
                        {{ Carbon\Carbon::parse($entry->created_at)->format('M d, Y h:i A') }}
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>
                    <style>
                        div.card-body:hover {
                          opacity: 1;
                        }
                    </style>

@endsection
