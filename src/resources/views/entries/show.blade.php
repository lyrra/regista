@extends('layouts.app')
@section('title', 'New Entry')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form>
                        @csrf

                        <div class="form-group row">
                            <label for="time-in" class="col-md-4 col-form-label text-md-right">Time In</label>

                            <div class="col-md-6">
                                <input id="time-in" type="datetime-local" readonly class="form-control" name="start_time" value="{{ Carbon\Carbon::parse($entry->start_time)->format('Y-m-d\TH:i') }}">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="time-out" class="col-md-4 col-form-label text-md-right">Time Out</label>

                            <div class="col-md-6">
                                <input id="time-out" type="datetime-local" readonly class="form-control" value="{{ Carbon\Carbon::parse($entry->end_time)->format('Y-m-d\TH:i') }}">
                                <!-- <input id="time-out" type="text" class="form-control" name="time-out" required> -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="textarea" class="col-md-4 col-form-label text-md-right">Purpose</label>

                            <div class="col-md-6">
                                <textarea readonly class="form-control" id="textarea" rows="3">{{ $entry->summary }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a href="/regista/entries/{{ $entry->id }}/edit" class="card-link">Edit</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
