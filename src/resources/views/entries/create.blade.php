@extends('layouts.app')
@section('title', 'New Entry')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="/regista/entries">
                        @csrf

                        <div class="form-group row">
                            <label for="time-in" class="col-md-4 col-form-label text-md-right">Start</label>

                            <div class="col-md-6">
                                <input id="start-time" type="datetime-local" class="form-control" name="start_time" value="{{Carbon\Carbon::now()->format('Y-m-d\TH:i')}}" required autofocus>
                                <!-- <input id="time-in" type="text" class="form-control" name="time-in" required autofocus> -->
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="end-time" class="col-md-4 col-form-label text-md-right">End</label>

                            <div class="col-md-6">
                                <input id="end-time" type="datetime-local" class="form-control" name="end_time" value="{{Carbon\Carbon::now()->format('Y-m-d\TH:i')}}">
                                <!-- <input id="time-out" type="text" class="form-control" name="time-out" required> -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="textarea" class="col-md-4 col-form-label text-md-right">Summary</label>

                            <div class="col-md-6">
                                <textarea class="form-control" id="textarea" rows="3" name="summary"></textarea>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
