@extends('layouts.app')
@section('title', 'New Entry')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="/regista/entries/{{ $entry->id }}" id="update-form">
                        @method('PATCH')
                        @csrf

                        <div class="form-group row">
                            <label for="time-in" class="col-md-4 col-form-label text-md-right">Time In</label>

                            <div class="col-md-6">
                                <input id="start-time" type="datetime-local" class="form-control" name="start_time" value="{{Carbon\Carbon::parse($entry->start_time)->format('Y-m-d\TH:i')}}">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="time-out" class="col-md-4 col-form-label text-md-right">Time Out</label>

                            <div class="col-md-6">
                                <input id="end-time" type="datetime-local" class="form-control" name="end_time" value="{{Carbon\Carbon::parse($entry->end_time)->format('Y-m-d\TH:i')}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="textarea" class="col-md-4 col-form-label text-md-right">Purpose</label>

                            <div class="col-md-6">
                                <textarea class="form-control" id="textarea" rows="3" name="summary">{{ $entry->summary }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-4"></div>
                            <div class="col">

                                <button type="submit" class="btn btn-primary" form="update-form">
                                    Update
                                </button>
                                
                            </div>
                            <div class="col text-right">
                                <button type="button" class="btn btn-danger" onclick="document.formdelete.submit()">
                                    Delete
                                </button>                        
                            </div>
                            <div class="col-md-2"></div>
                        </div>
<!-- 
                        <div class="form-group row mb-0">
                            <div class="col-md-4 alert-info"></div>
                            <div class="col alert-success">Button</div>
                            <div class="col text-right alert-danger">Button</div>
                            <div class="col-md-2 alert-info"></div>
                        </div>
 -->
                    </form>

                    <form method="POST" action="/regista/entries/{{ $entry->id }}" name="formdelete">
                        @method('DELETE')
                        @csrf
                    </form>

<!--                     <form method="POST" action="/regista/entries/{{ $entry->id }}">
                        @method('DELETE')
                        @csrf

                        <div class="form-group row mb-0 text-right">
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-danger">
                                    Delete
                                </button>
                                
                            </div>
                        </div>
                    </form> -->
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
