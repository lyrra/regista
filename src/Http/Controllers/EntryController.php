<?php

namespace Hazizurin\Regista\Http\Controllers;

use App\Http\Controllers\Controller;
use Hazizurin\Regista\Models\Entry;
use Illuminate\Http\Request;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = Entry::where('person_id', auth()->id())->latest('start_time')->get();
        // $entries = Entry::latest('start_time')->get();
        // $entries = Entry::oldest('start_time')->get();
        // $entries = Entry::orderBy('start_time', 'desc')->get();
        // $entries = Entry::all();
        
        // $this->authorize('view', $entries);

        return view('regista::entries.index', compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('regista::entries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->all();

        $attributes['person_id'] = auth()->id();

        Entry::create($attributes);
        // Entry::create(request()->all());

        return redirect('/regista/entries')->with('status', 'Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function show(Entry $entry)
    {

        $this->authorize('view', $entry);
        // abort_if($entry->person_id !== auth()->id(), 403);

        return view('regista::entries.show', compact('entry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function edit(Entry $entry)
    {
        return view('regista::entries.edit', compact('entry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entry $entry)
    {
        $entry->update(request()->all());

        return redirect('/regista/entries')->with('status', 'Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entry)
    {
        $entry->delete();

        return redirect('/regista/entries')->with('status', 'Deleted.');
    }
}
