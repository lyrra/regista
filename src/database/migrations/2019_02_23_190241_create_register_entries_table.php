<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('person_id');
            $table->timestamp('start_time');
            $table->timestamp('end_time')->nullable();
            $table->text('summary')->nullable();
            $table->timestamps();

            $table->foreign('person_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_entries');
    }
}
