<?php

namespace Hazizurin\Regista\Models;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $fillable = ['person_id', 'start_time', 'end_time', 'summary'];
    protected $table = 'register_entries';

    public function Person()
    {
    	return $this->belongsTo('App\User');
    }
}
