<?php

namespace Hazizurin\Regista;

// use Illuminate\Support\ServiceProvider;
use App\Providers\AuthServiceProvider as ServiceProvider;

class RegistaServiceProvider extends ServiceProvider {
	protected $policies = [
		'Hazizurin\Regista\Models\Entry' => 'Hazizurin\Regista\Policies\EntryPolicy',
	];

    public function register() {

    }

    public function boot() {
        $this->registerPolicies();
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'regista');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}