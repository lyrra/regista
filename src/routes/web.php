<?php

Route::group(['namespace' => 'Hazizurin\Regista\Http\Controllers', 'middleware' => ['web', 'auth']], function(){
        Route::get('/regista', function () {
            return view('regista::index');
        });

        Route::resource('/regista/entries', 'EntryController');
});